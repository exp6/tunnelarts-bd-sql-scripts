USE [TunnelArts]
GO
INSERT [Characterization].[Air] ([id], [code], [description]) VALUES (N'c0392a70-48bb-4f30-8d03-0dda3847406d', 1, N'Poor                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Air] ([id], [code], [description]) VALUES (N'a4389451-eebc-4a8b-bdb0-1081c9184b41', 2, N'Fair                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Air] ([id], [code], [description]) VALUES (N'ac97736a-2ff1-4e7e-9e3b-96d62c4feaa8', 3, N'Good                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[AlterationWeathering] ([id], [code], [description]) VALUES (N'1467baff-6a20-457b-83a6-3e12d3753be7', 0, N'Fresh [0]                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[AlterationWeathering] ([id], [code], [description]) VALUES (N'c75b5cca-1cc0-4b8a-9571-5ddbde00c442', 1, N'Slightly weathered [I]                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[AlterationWeathering] ([id], [code], [description]) VALUES (N'6b1f2910-adef-45fc-a083-61fae5fd2cde', 5, N'Residual soil [V]                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[AlterationWeathering] ([id], [code], [description]) VALUES (N'51ea1ec1-03f3-48db-b73d-71b2f01fa2e7', 4, N'Completely weathered [IV]                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[AlterationWeathering] ([id], [code], [description]) VALUES (N'e6c7097b-8e1b-4db3-a5a0-a5803fdfa5fe', 2, N'Moderately weathered [II]                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[AlterationWeathering] ([id], [code], [description]) VALUES (N'8bedddb3-6b1c-4f6c-9e7a-afe5d9bcdcc4', 3, N'Highly weathered [III]                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[BlockShape] ([id], [code], [description]) VALUES (N'400c5c62-1f23-41fa-a544-028cf3d277f6', 6, N'Columnar blocks                                                                                                                                                                                                                                                ')
GO
INSERT [Characterization].[BlockShape] ([id], [code], [description]) VALUES (N'2305520e-ca4c-4b34-adb9-1bab26127ee2', 5, N'Rhomboidal blocks                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[BlockShape] ([id], [code], [description]) VALUES (N'd99dc10a-020f-485d-a45a-4a69c9066ae1', 3, N'Prismatic blocks                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[BlockShape] ([id], [code], [description]) VALUES (N'0752212d-7a2f-4be7-85d9-636a18a195df', 4, N'Equidimensional blocks                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[BlockShape] ([id], [code], [description]) VALUES (N'7c88056b-619b-4ddd-b7ac-a8f9bf1b4de5', 1, N'Polyhedral blocks                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[BlockShape] ([id], [code], [description]) VALUES (N'd3426993-1e53-4b9c-8d47-dd02fae4d834', 2, N'Tabular blocks                                                                                                                                                                                                                                                 ')
GO
INSERT [Characterization].[BlockShapeType] ([id], [code], [description]) VALUES (N'b6421f17-a97a-405f-976b-096608945f09', 4, N'Elongated                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[BlockShapeType] ([id], [code], [description]) VALUES (N'3db45085-f232-4107-9631-42c34b77fbb9', 2, N'Cubic                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[BlockShapeType] ([id], [code], [description]) VALUES (N'8c850867-481e-4d8f-83ae-d30ba9249b35', 3, N'Flat                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[BlockShapeType] ([id], [code], [description]) VALUES (N'cf7eb402-266e-4086-b33f-d88d7b92e207', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'64bb83a9-af88-4d73-8b06-072a219446ad', 13, N'Fine silt (FSi) [0,002<s<0,0063]                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'fa5c9216-7b68-42d5-9d28-11cb6f757f47', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'54679106-3d0b-4351-8260-1474b5471efa', 11, N'Coarse silt (CSi) [0,02<s<0,063]                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'9fb295b3-08f0-43b3-9332-1a7d0d35a788', 3, N'Boulder (Bo) [200<s<630]                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'fcfdff3f-f24f-44f5-89e3-2ef2a1ae1a15', 14, N'Clay (Cl) [s<0,002]                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'b190622a-0b3a-403e-8bad-46a8b85b852e', 10, N'Fine sand (FSa) [0,063<s<0,2]                                                                                                                                                                                                                                  ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'c6dcfe28-c986-4755-9647-5329e5f2e003', 7, N'Fine gravel (FGr) [2<s<6,3]                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'80aead2a-1388-4e7b-aae7-77ff8a31b9fd', 9, N'Medium sand (MSa) [0,2<s<0,63]                                                                                                                                                                                                                                 ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'a0931c63-fd4c-4453-b471-842731cb1cc1', 12, N'Medium silt (MSi) [0,0063<s<0,02]                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'202e845f-2654-4bb4-b0f4-9ca91b71cbae', 5, N'Coarse gravel (CGr) [20<s<63]                                                                                                                                                                                                                                  ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'4f1f48a3-a852-46fa-b54f-b949a54ec904', 8, N'Coarse sand (CSa) [0,63<s<2]                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'0f07011d-9f56-450d-813b-bf7baacfef08', 4, N'Cobble (Co) [63<s<200]                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'ed4f3f13-3d1a-4c35-9bb3-f890c928d449', 2, N'Large boulder (Lbo) [630<s]                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[BlockSize] ([id], [code], [description]) VALUES (N'bb82b74d-f4aa-4925-b3ab-fac629b20271', 6, N'Medium gravel (MGr) [6,3<s<20]                                                                                                                                                                                                                                 ')
GO
INSERT [Characterization].[ColourType] ([id], [code], [description]) VALUES (N'34adf7e2-d82f-4c67-a32d-136d833f197f', 1, N'Chroma                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[ColourType] ([id], [code], [description]) VALUES (N'1a1f909e-8737-4624-bfc8-85bfae73c446', 0, N'Value                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[ColourType] ([id], [code], [description]) VALUES (N'06c9d693-e047-4b9e-ba21-a4b260bed6e1', 2, N'Hue                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'74956ba7-aad2-4745-8aed-01b8480b544b', 10, N'Grey                                                                                                                                                                                                                                                           ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'c5229e2a-e8a2-4d65-a020-049d88581e7b', 3, N'Yellowish                                                                                                                                                                                                                                                      ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'e2caf967-50cb-4aed-a8ab-04f9823ee996', 7, N'Bluish                                                                                                                                                                                                                                                         ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'2f41d939-7f1c-4d40-b51a-11af2f6d450f', 5, N'Brownish                                                                                                                                                                                                                                                       ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'3a3d8a8c-3f58-48d3-b914-1cbbc8aa4b07', 8, N'Purple                                                                                                                                                                                                                                                         ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'ba969e85-e03a-45c1-aa77-2ccacf156239', 2, N'Reddish                                                                                                                                                                                                                                                        ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'758c92d8-0e72-402e-b52b-2efea054e391', 4, N'Orange                                                                                                                                                                                                                                                         ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'f10858d6-ab2d-4c04-8d06-44f4291cd379', 11, N'Black                                                                                                                                                                                                                                                          ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'e0ba1055-11f2-4137-a8be-499cbb4e7565', 1, N'Pink                                                                                                                                                                                                                                                           ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'd0902a8a-a912-45c0-94f9-548f61b1a644', 6, N'Greenish                                                                                                                                                                                                                                                       ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'8aa7030a-46d1-47c0-96ce-60d5b1ffc75b', 2, N'Dark                                                                                                                                                                                                                                                           ', 0)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'd36b20ac-7c5a-4127-98e1-7e65ca9c4a76', 1, N'Pinkish                                                                                                                                                                                                                                                        ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'31688c96-ec2d-438b-a356-83a2d297567d', 4, N'Orangish                                                                                                                                                                                                                                                       ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'd5991246-0491-49a3-81fb-859f48b0b4ea', 1, N'Light                                                                                                                                                                                                                                                          ', 0)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'e4bb6273-6949-4ef2-b2ea-ce0dacdd3782', 2, N'Red                                                                                                                                                                                                                                                            ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'b7b5f483-234a-469e-8cb7-d61f43fa2bf8', 8, N'Purplish                                                                                                                                                                                                                                                       ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'c3fbe72b-d33f-4483-b01b-d9acf8c2fde2', 3, N'Yellow                                                                                                                                                                                                                                                         ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'8725a525-9bb2-499e-bfe7-da93d0100f5e', 6, N'Green                                                                                                                                                                                                                                                          ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'a719a23a-8a6f-451c-986f-e9e36fedc8c8', 9, N'White                                                                                                                                                                                                                                                          ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'affe4f24-0f72-4c18-82dc-ea5e73cb9070', 9, N'Greyish                                                                                                                                                                                                                                                        ', 1)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'98d2d181-5ce2-482c-aff5-eab2b4da8142', 7, N'Blue                                                                                                                                                                                                                                                           ', 2)
GO
INSERT [Characterization].[ColourTypeValue] ([id], [code], [description], [idColourType]) VALUES (N'17ac4651-b4fe-471a-9b58-f063526fadb7', 5, N'Brown                                                                                                                                                                                                                                                          ', 2)
GO
INSERT [Characterization].[Damage] ([id], [code], [description]) VALUES (N'c315acf1-5417-454e-a3d5-6cdf2ed369e5', 1, N'Yes                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[Damage] ([id], [code], [description]) VALUES (N'3d577626-d4e9-4fb5-8b00-78e1e8d17706', 2, N'No                                                                                                                                                                                                                                                             ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'0b2a7601-5dbb-4e0f-b212-258e83321ec2', 6, N'Bedding Plane (B)                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'052ed3a7-85cd-4f03-bb0e-26e3f6f42ca3', 9, N'Fold (Fo)                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'df1f5afe-98ff-47e2-abb0-37288309f22e', 1, N'Fault (F)                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'59265664-4d67-4b65-a26c-59a4074a795e', 3, N'Slickenside (SI)                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'be9b71a3-cb75-4b42-81e2-5ac424eaafb0', 7, N'Foliation (FL)                                                                                                                                                                                                                                                 ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'9cbcb7b1-84cc-4b41-a21c-78bb71b93544', 8, N'Shear (Sn)                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'ca56f66b-62b7-4024-ade6-85d22a876b57', 4, N'Fractured (Fr)                                                                                                                                                                                                                                                 ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'5ea4de95-237d-4bbf-868b-aa42439b8271', 2, N'Joint (J)                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[DiscontinuityType] ([id], [code], [description]) VALUES (N'f71842cb-dc61-4288-8ceb-c06a8a7579af', 5, N'Schistosity (SC)                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[DistanceToFace] ([id], [code], [description]) VALUES (N'fe2ef8e1-5c41-42e7-9063-4b70fdc0da5c', 1, N'd < 3 [m]                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[DistanceToFace] ([id], [code], [description]) VALUES (N'8bc5c383-4e5f-4900-86c2-ae95da9584c4', 3, N'6 < d [m]                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[DistanceToFace] ([id], [code], [description]) VALUES (N'2a87d099-417e-4da5-b4a9-fa115f33ac07', 2, N'3 < d < 6 [m]                                                                                                                                                                                                                                                  ')
GO
INSERT [Characterization].[FaceType] ([id], [code], [description]) VALUES (N'7649443c-6f9a-424a-ad76-925968f4275c', 1, N'3 < d < 6 [m]                                                                                                                                                                                                                                                  ')
GO
INSERT [Characterization].[FaceType] ([id], [code], [description]) VALUES (N'fc9dfb64-f88c-49ba-adfc-e6911f3eaa62', 0, N'd < 3 [m]                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[FaceType] ([id], [code], [description]) VALUES (N'b1563c16-1ebc-4f62-9547-efa61c922409', 2, N'6 < d [m]                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[FaultType] ([id], [code], [description]) VALUES (N'83bfbc60-5502-473f-b2d0-012d2870bfdc', 0, N'Normal                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[FaultType] ([id], [code], [description]) VALUES (N'a65a495b-2712-4fb1-9037-22966866e223', 4, N'Strike-Slip (Dextral)                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[FaultType] ([id], [code], [description]) VALUES (N'cc55104f-637d-47ea-a93e-303bdf328660', 3, N'Strike-Slip (Sinistral)                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[FaultType] ([id], [code], [description]) VALUES (N'157eee69-b68b-433b-94ed-4257ce0fc509', 2, N'Oblique                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[FaultType] ([id], [code], [description]) VALUES (N'0f550499-cc15-4267-b750-4dcc18a8f659', 5, N'Rotational (Hingle)                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[FaultType] ([id], [code], [description]) VALUES (N'e817d10f-fdb7-46e8-b0c5-6c551d3d0e84', 1, N'Reverse                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[FaultType] ([id], [code], [description]) VALUES (N'3ec7b803-9399-4e59-91dc-98ff963d865c', 6, N'Scissor (Pivot)                                                                                                                                                                                                                                                ')
GO
INSERT [Characterization].[GeneticGroup] ([id], [code], [description]) VALUES (N'399e35c3-6bcd-44b7-80d5-0f092e528544', 5, N'Sedimentary Clastic                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[GeneticGroup] ([id], [code], [description]) VALUES (N'a4b22249-0fb7-432a-8970-3c1f7ee0ad8b', 2, N'Igneous Plutonic                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[GeneticGroup] ([id], [code], [description]) VALUES (N'a25a604a-e3a9-4f05-9aaa-48974d2a5784', 4, N'Pyroclastic                                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[GeneticGroup] ([id], [code], [description]) VALUES (N'3c776c08-3485-4027-aa75-6d708db31e8e', 3, N'Igneous Volcanic                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[GeneticGroup] ([id], [code], [description]) VALUES (N'19159ef9-3797-4de6-9ee1-b1dd4cddeb5d', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[GeneticGroup] ([id], [code], [description]) VALUES (N'6ed66405-fff7-481e-87a5-d26ec4e565c2', 6, N'Sedimentary Chemical                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[GeneticGroup] ([id], [code], [description]) VALUES (N'7140982f-04ed-4904-9af2-f77b616383c2', 7, N'Metamorphic                                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[GrainSize] ([id], [code], [description]) VALUES (N'a29644e0-a819-4b3f-9605-1d4c11944549', 1, N'Fine-grained [0,002-0,063 mm]                                                                                                                                                                                                                                  ')
GO
INSERT [Characterization].[GrainSize] ([id], [code], [description]) VALUES (N'cddb3628-3dec-437c-b972-480dc09e9261', 3, N'Coarse-grained [2-63 mm]                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[GrainSize] ([id], [code], [description]) VALUES (N'97461887-e7c0-4e30-9280-6fe83d011b1a', 4, N'Very coarse-grained [>63 mm]                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[GrainSize] ([id], [code], [description]) VALUES (N'a2fcffd3-1af2-4388-a138-a9eb83ec736b', 2, N'Medium-grained [0,063-2 mm]                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[Infilling] ([id], [code], [description]) VALUES (N'627ba361-739e-4bd7-a321-0c4bd80d5a8f', 3, N'Hard >5 mm                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[Infilling] ([id], [code], [description]) VALUES (N'b438212b-82fa-4709-8eb9-4253a9750da4', 4, N'Soft <5 mm                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[Infilling] ([id], [code], [description]) VALUES (N'c8bbe9c8-c895-4243-833a-562f8f4003d6', 1, N'None                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Infilling] ([id], [code], [description]) VALUES (N'4d4307d9-d11c-4bde-9c31-c0c683352da3', 2, N'Hard <5 mm                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[Infilling] ([id], [code], [description]) VALUES (N'1e166e9f-6209-4a69-8439-c9d30c3c83e7', 5, N'Soft >5 mm                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'28b0fb5a-80bb-4610-a601-0290e732a920', 2, N'Sand (Sa) [soft]                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'bd874378-a531-48b7-ac59-18cfae6bf8f7', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'ed1e7496-3cc6-4c49-8351-1d4c3d2a7589', 9, N'Talc (T) [Mineral]                                                                                                                                                                                                                                             ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'528f1ada-f4c3-4cd5-bd47-1e49b656d282', 8, N'Epidote (Ep) [Mineral]                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'7aae8ab8-66f5-47e6-89b0-2a60fa85f5fb', 22, N'Zeolite (Zeo) [Mineral]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'44cc3621-9d2d-4a97-9f32-352641ed6318', 17, N'Turmalin (Tm) [Mineral]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'98886323-9f4c-488d-9d34-472ed102df5d', 19, N'Specularite (Sr) [Mineral]                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'354b4b72-0c8f-4ca8-bd14-61f2e3b0f28e', 20, N'Plagioclase (Plag) [Mineral]                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'34d1d8da-3118-44fb-8e0e-628ce1c2aa79', 14, N'Limonite (Lm) [Mineral]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'8de2ba1b-c0bd-44e3-8902-7091efc7efb8', 10, N'Pyrite (Py) [Mineral]                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'2a63be37-67d7-40a2-99a2-8af28d9c8871', 18, N'Hematite (He) [Mineral]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'a385af94-68dd-4215-8e43-8f55ef41bbf9', 3, N'Silt (Si) [soft]                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'23e46d7c-ccc4-4045-8587-96a95a81a24d', 13, N'Gypsum (Y) [Mineral]                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'51fef196-8a9c-4fca-9b61-aacd48db2411', 16, N'Feldespar (Fd) [Mineral]                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'67128eef-a674-49df-8542-b410c90aa22d', 15, N'Quartz (Qz) [Mineral]                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'2cc80f48-bd8b-4060-8337-b97c71c91db1', 11, N'Copper (Cu) [Mineral]                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'bf4033c9-92fd-4a90-87b6-c7fbed19152b', 21, N'Anhydrite (An) [Mineral]                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'48b495c2-9aac-46cc-834a-cb5d98c28a73', 4, N'Clay (Cl) [Soft]                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'182a411c-eef2-4037-9330-cb97a23b038a', 7, N'Sericite (Sc) [Mineral]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'03cc704f-972c-40bf-8664-d9a39e51497c', 6, N'Chlorite (Cl) [Mineral]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'5900ee18-87d2-4c17-ae09-dffc87aebf56', 5, N'Calcite (Ca) [Mineral]                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[InfillingType] ([id], [code], [description]) VALUES (N'6dfd62a2-67bd-4fa7-930c-f645df1445c4', 12, N'Chalcopyrite (Cpy) [Mineral]                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[JointingType] ([id], [code], [description]) VALUES (N'13b824e6-e3d9-4e44-ba72-03b860bdf963', 1, N'Very low [<1/m3]                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[JointingType] ([id], [code], [description]) VALUES (N'98fd52f7-5dcd-4703-93b4-1826f21fa305', 4, N'High [10-30/m3]                                                                                                                                                                                                                                                ')
GO
INSERT [Characterization].[JointingType] ([id], [code], [description]) VALUES (N'781afe67-e254-4437-9322-2efd55d21348', 2, N'Low [1-3/m3]                                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[JointingType] ([id], [code], [description]) VALUES (N'f783ab8a-8647-4261-b401-329595214883', 3, N'Moderate [3-10/m3]                                                                                                                                                                                                                                             ')
GO
INSERT [Characterization].[JointingType] ([id], [code], [description]) VALUES (N'6ff6d5f7-e46b-44e0-8427-bd65a1e7b99d', 5, N'Very high [30-60/m3]                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[JointingType] ([id], [code], [description]) VALUES (N'3cb45a13-0cf5-469a-8fcd-dbf8772242c6', 6, N'Crushed [>60/m3]                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[Light] ([id], [code], [description]) VALUES (N'f20362f6-24a8-4edf-bc77-08b538e7d71b', 3, N'Good                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Light] ([id], [code], [description]) VALUES (N'a4695d0d-a492-42b2-a2b8-19a0444c55da', 2, N'Fair                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Light] ([id], [code], [description]) VALUES (N'57de90ed-382a-42fa-8f1f-6229c70522ac', 1, N'Poor                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'992c6a3d-aa0c-4d20-a90d-13c1eb629160', 7, N'Very wide [10 to 100 cm]                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'ed31fbb7-6744-45ab-af1e-1c2af330c0a9', 3, N'Partly open [0,25 to 0,5 mm]                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'585a20e0-aaf2-47a8-b65b-489f71db428a', 6, N'Wide [1 to 10 cm]                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'31d3fd63-5be8-436d-b3ff-5ac4815c9fcf', 4, N'Open [0,5 to 2,5 mm]                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'742052b0-61f4-4733-8240-69b5d3d2eaac', 2, N'Tight [0,1 to 0,25 mm]                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'c8e51bdd-b6ca-4db1-b3cd-6bd9eece9a2a', 8, N'Extremely wide [> 1 m]                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'e0a1138a-be5f-4133-91a2-8f5f09424e5b', 5, N'Moderately wide [2,5 to 10 mm]                                                                                                                                                                                                                                 ')
GO
INSERT [Characterization].[Opening] ([id], [code], [description]) VALUES (N'd0de08e1-cf4a-45b8-ae85-96009f6cae38', 1, N'Very tight [< 0,1 mm]                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[OverbreakCauses] ([id], [code], [description]) VALUES (N'024df3b4-0799-4adf-9016-03ae475aeed5', 6, N'Excesive span                                                                                                                                                                                                                                                  ')
GO
INSERT [Characterization].[OverbreakCauses] ([id], [code], [description]) VALUES (N'a85f36b3-06f7-4fae-89bd-46eb655cb943', 5, N'Unadequate drilling                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[OverbreakCauses] ([id], [code], [description]) VALUES (N'fbde12a5-51f9-4351-8400-5c4d2869da97', 1, N'Geology - Fault Zone                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[OverbreakCauses] ([id], [code], [description]) VALUES (N'36ac4c8c-d538-49a3-8179-7b6467dcb48a', 4, N'Excesive charge factor                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[OverbreakCauses] ([id], [code], [description]) VALUES (N'bb61b65c-5956-446c-adb9-805f5c13bb25', 3, N'Geology - In situ stress                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[OverbreakCauses] ([id], [code], [description]) VALUES (N'f6d7abf5-e1ad-4b00-89f8-ee3dd204b2b1', 2, N'Geology - Wedge Failure                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[Persistence] ([id], [code], [description]) VALUES (N'34705286-ce2d-4690-a8c0-1294ca04b338', 4, N'3 - 10 m                                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[Persistence] ([id], [code], [description]) VALUES (N'6b7fe0b3-9a4c-48f4-9997-2af840ace5af', 3, N'1 - 3 m                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[Persistence] ([id], [code], [description]) VALUES (N'01b83a08-8830-44a1-a3df-3c8400215f5e', 2, N'< 1 m                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Persistence] ([id], [code], [description]) VALUES (N'715ce190-cb81-4c03-9138-4caf57275df7', 6, N'> 20 m                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[Persistence] ([id], [code], [description]) VALUES (N'75d016d7-86b2-4bc9-aa1f-57c01cef6fd3', 5, N'10 - 20 m                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[Persistence] ([id], [code], [description]) VALUES (N'a7c7d05d-d640-490f-ad5c-6198851e4c80', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[QJnType] ([id], [code], [description]) VALUES (N'6cbb6273-3e76-4fdc-af71-010dca4dd5ab', 0, N'None                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[QJnType] ([id], [code], [description]) VALUES (N'bbec5da0-ace1-4f1e-9b51-31ec2c955b9c', 1, N'Intersection                                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[QJnType] ([id], [code], [description]) VALUES (N'8d6a6a36-a915-45c3-827f-6b5056c6fe20', 2, N'Portal                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'a48c0b62-17bc-4300-8f4c-08b54b807dbe', 7, N'Extremmely good                                                                                                                                                                                                                                                ', 400, 100)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'4450ad68-747e-48c8-96a9-1a98a34aafe2', 2, N'Very poor                                                                                                                                                                                                                                                      ', 1, 0.1)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'ca972dd4-b320-4c47-9e24-38b321bf18bd', 0, N'Exceptionally poor                                                                                                                                                                                                                                             ', 0.01, 0)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'9ee92bb0-1461-4b76-a101-3f705c8155ef', 6, N'Very good                                                                                                                                                                                                                                                      ', 100, 40)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'f5f11536-44dd-4837-b819-5b084f2977b2', 1, N'Extremmely poor                                                                                                                                                                                                                                                ', 0.1, 0.01)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'108a7151-83a2-498a-a51d-89ccc5ea227f', 3, N'Poor                                                                                                                                                                                                                                                           ', 4, 1)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'c5b2a531-da5c-428b-9ac0-8cf5e19d1398', 4, N'Fair                                                                                                                                                                                                                                                           ', 10, 4)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'e2337eeb-1d5b-4feb-9391-987ec271083f', 8, N'Exceptionally good                                                                                                                                                                                                                                             ', 401, 400)
GO
INSERT [Characterization].[QRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound]) VALUES (N'be8dda68-5eea-4b5c-84c1-de4f069edcf3', 5, N'Good                                                                                                                                                                                                                                                           ', 40, 10)
GO
INSERT [Characterization].[Reason] ([id], [code], [description]) VALUES (N'3fb0349a-fe5e-4acf-96e2-285e73dec415', 4, N'Other                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Reason] ([id], [code], [description]) VALUES (N'5957dac4-0120-4ece-b997-4f044fae2f1f', 1, N'Water                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Reason] ([id], [code], [description]) VALUES (N'e539cdd8-da60-4829-9e0c-7df52298287d', 2, N'Scaling                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[Reason] ([id], [code], [description]) VALUES (N'5a1c2196-e4a2-4e86-b2c7-a4aa3dfebbb9', 3, N'Adverse rock                                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[Relevance] ([id], [code], [description]) VALUES (N'f4073003-0e47-452d-ac40-40e6a71a22fb', 3, N'Terciary                                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[Relevance] ([id], [code], [description]) VALUES (N'ead87d49-75ff-4f46-9930-55c8f418e4fc', 4, N'Random                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[Relevance] ([id], [code], [description]) VALUES (N'e1c9c2ed-9b6a-4d74-aaec-757522aab583', 1, N'Primary                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[Relevance] ([id], [code], [description]) VALUES (N'41c0c9ca-edf3-46ed-9ba6-e961e95e74c3', 2, N'Secondary                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[Responsible] ([id], [code], [description]) VALUES (N'575c1314-0879-4d15-8307-81d3dbb634d8', 4, N'Engineer                                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[Responsible] ([id], [code], [description]) VALUES (N'a12bd5a3-f697-4571-ae0d-bd7d39f0127f', 2, N'Owner                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Responsible] ([id], [code], [description]) VALUES (N'f56b60c9-6cba-445b-a8be-e2ab787d4826', 1, N'Inspection                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[Responsible] ([id], [code], [description]) VALUES (N'9a92274f-7352-47e6-bf51-e6b076469cb5', 3, N'Contractor                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[RmrRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound], [Classification]) VALUES (N'd23e4676-6f7f-4e50-b377-044b6ebc8633', 2, N'Fair rock                                                                                                                                                                                                                                                      ', 60, 40, N'Class III                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[RmrRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound], [Classification]) VALUES (N'59d9a626-1988-4790-80a6-1ae88527ebb9', 3, N'Good rock                                                                                                                                                                                                                                                      ', 80, 60, N'Class II                                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[RmrRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound], [Classification]) VALUES (N'082e60b8-83c2-436f-9113-2789051d8426', 4, N'Very good rock                                                                                                                                                                                                                                                 ', 100, 80, N'Class I                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[RmrRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound], [Classification]) VALUES (N'53ef4ef5-6e01-407e-8e2e-4f1e5a9f4a1e', 0, N'Very poor rock                                                                                                                                                                                                                                                 ', 20, 0, N'Class V                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[RmrRockQuality] ([id], [Code], [Name], [UpperBound], [LowerBound], [Classification]) VALUES (N'd005723a-952c-4a2d-b285-b4b3f8fc219d', 1, N'Poor rock                                                                                                                                                                                                                                                      ', 40, 20, N'Class IV                                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[Roughness] ([id], [code], [description]) VALUES (N'70125584-41c7-4be3-8512-0df02cf3dc64', 5, N'Planar rough surface                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Roughness] ([id], [code], [description]) VALUES (N'b90c5243-b27c-4ee8-b935-4ceb306b6086', 6, N'Planar smooth surface                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Roughness] ([id], [code], [description]) VALUES (N'27ccf55d-ba40-4abb-a38c-9871e903f584', 2, N'Stepped smooth surface                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[Roughness] ([id], [code], [description]) VALUES (N'e651a00b-6860-4617-a215-a30f541d1336', 4, N'Undulating smooth surface                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[Roughness] ([id], [code], [description]) VALUES (N'c0855e13-7a1a-4dfc-8cf4-b2e097901a4a', 1, N'Stepped rough surface                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Roughness] ([id], [code], [description]) VALUES (N'f5037f84-ca97-45b9-9a55-e944453b6ccb', 3, N'Undulating rough surface                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'61551e8d-8e0f-44b3-955e-2f9952995794', 3, N'Reverse                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'7b612a1a-6266-4c50-8ac7-4d0efc39877a', 6, N'Strike-Slip (Dextral)                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'f5f25aad-7c08-493b-9f3c-54123cd9028d', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'1add1cae-dd2a-4c9c-9359-930039eab50f', 2, N'Normal                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'c3533345-e9cc-44f4-ad65-a11b6ff2ad24', 8, N'Scissor (Pivot)                                                                                                                                                                                                                                                ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'70e94253-4135-4f90-8fff-cebc85c4fad7', 5, N'Strike-Slip (Sinistral)                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'c71c2451-ca3b-4777-96f3-d265639fe640', 4, N'Oblique                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[SenseMovement] ([id], [code], [description]) VALUES (N'2e8570af-a9f4-41c3-967f-faa538c08c85', 7, N'Rotational (Hingle)                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[Slickensided] ([id], [code], [description]) VALUES (N'85971d0f-68f0-4836-86eb-18841efd0090', 2, N'No                                                                                                                                                                                                                                                             ')
GO
INSERT [Characterization].[Slickensided] ([id], [code], [description]) VALUES (N'57037d6c-0fda-419c-b778-b9f40019973c', 1, N'Yes                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[Spacing] ([id], [code], [description]) VALUES (N'71b29b33-531a-49c3-90d3-0ecb71f76dca', 5, N'Small [6 to 20 cm]                                                                                                                                                                                                                                             ')
GO
INSERT [Characterization].[Spacing] ([id], [code], [description]) VALUES (N'e0600916-6313-49ba-ba51-29f4f01658dd', 6, N'< 60 mm                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[Spacing] ([id], [code], [description]) VALUES (N'1302501c-bef2-46de-9052-7ba0a0070a11', 4, N'Medium [0,2 to 0,6 m]                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Spacing] ([id], [code], [description]) VALUES (N'e9abf74e-974f-43a3-8d0b-86e8c5fe80b2', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[Spacing] ([id], [code], [description]) VALUES (N'ed203914-2805-4fd3-8662-9a08d95dd7ea', 2, N'Very wide [> 2 m]                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[Spacing] ([id], [code], [description]) VALUES (N'a9323d5a-204c-4666-abe4-cadf97653a65', 3, N'Wide [0,6 to 2 m]                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[Strength] ([id], [code], [description]) VALUES (N'00636822-0b87-422c-9706-6710d777ec17', 2, N'Very weak [1-5 MPa]                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[Strength] ([id], [code], [description]) VALUES (N'ab5c04a8-9ea2-4158-a4e9-96f2f21f30e7', 7, N'Extremely strong [>250 MPa]                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[Strength] ([id], [code], [description]) VALUES (N'b09312d4-e148-4595-833a-d230ab24354f', 1, N'Extremely weak [<1 MPa]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[Strength] ([id], [code], [description]) VALUES (N'6b0f19e5-ed20-4505-ac3a-d7868993c57e', 5, N'Strong [50-100 MPa]                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[Strength] ([id], [code], [description]) VALUES (N'e5654796-a1a2-4233-b902-debc4be9996f', 4, N'Medium strong [25-50 MPa]                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[Strength] ([id], [code], [description]) VALUES (N'fc6779ff-a913-419d-9325-efe317d30fb8', 6, N'Very strong [100-250 MPa]                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[Strength] ([id], [code], [description]) VALUES (N'361a518c-3434-4068-a98a-fb5e2a80644f', 3, N'Weak [5-25 MPa]                                                                                                                                                                                                                                                ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'a5511e0e-ee77-4c4a-ae19-15bc4ba182ca', 8, N'Gneissose                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'3f7e95f7-f61c-408e-a5a6-15d8ea386cd3', 14, N'Schistose                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'74daee75-e93d-4fe8-b6c0-2517f41ffe63', 15, N'Vein                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'5826f8d9-c3f7-441d-936d-376b80b4f6c3', 1, N'Banded                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'7f97fca9-8212-4940-a545-4c7b67ef6760', 12, N'Lineated                                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'141a965c-ab4d-4352-9b9b-771ece76c55c', 3, N'Cleaved                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'a2dda235-c791-4d87-94b6-7a1431ce27f3', 10, N'Interbedded                                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'58156e5a-8b15-4913-b32e-9be49c70e726', 2, N'Bedded                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'851c9736-26e6-4070-b5c8-9e72e6592486', 7, N'Foliated                                                                                                                                                                                                                                                       ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'd381bb81-4308-4062-b43a-a431f4849a3d', 4, N'Dyke                                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'c1ec250c-7282-4e92-92c9-c7559a0c9ddb', 11, N'Laminated                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'7f610d63-0696-4267-b016-c7ff99248256', 9, N'Graded                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'aa5d90e7-22ad-4130-89b3-dec36c1a65bc', 13, N'Massive                                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'd35c5934-d542-4daf-a049-f6c61e721ab5', 5, N'Flowbanded                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[StructureType] ([id], [code], [description]) VALUES (N'5b6ae6d7-ac16-4f87-a99d-ff7257f783c4', 6, N'Folded                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'5024d643-4aad-47c8-9289-03d436139bab', 4, N'Tonalite                                                                                                                                                                                                                                                       ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'0406e127-ee13-43c3-a735-0dd87a67e276', 14, N'Basalt                                                                                                                                                                                                                                                         ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'bf1f655b-937a-422f-a37b-10c8586b1498', 12, N'Latite                                                                                                                                                                                                                                                         ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'39f470be-f357-4df7-b9ba-11fd2afd8212', 10, N'Alkali trachyte                                                                                                                                                                                                                                                ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'eaeda768-ba19-4f01-9e86-133ca703c671', 4, N'Gneiss                                                                                                                                                                                                                                                         ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'a4702fd9-9c43-47aa-a032-148834d3166f', 6, N'Quartz syenite                                                                                                                                                                                                                                                 ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'48e47dd5-2349-4f95-b2cb-1760da151951', 1, N'Conglomerate                                                                                                                                                                                                                                                   ', 5)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'97d13f2d-e13c-4f07-953e-199e1a7b0acd', 15, N'Monzodiorite                                                                                                                                                                                                                                                   ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'cbd33992-3b58-46a3-a669-235169973034', 9, N'Quartz monzogabbro                                                                                                                                                                                                                                             ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'e36aecd2-3db3-4ef2-8e29-28a93a9b1890', 2, N'Phyllite                                                                                                                                                                                                                                                       ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'08702beb-4078-40ae-9594-2997ab737ef9', 3, N'Schist                                                                                                                                                                                                                                                         ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'6b19e148-9875-4571-aeae-2a5a361a18b7', 18, N'Pyroxenite                                                                                                                                                                                                                                                     ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'5d033c3e-8a02-4fe4-9314-2e3feb1efdc6', 8, N'Quartz monzodiorite                                                                                                                                                                                                                                            ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'61a4391f-d0de-49ea-bebc-2ed5e3636fa8', 17, N'Gabbro                                                                                                                                                                                                                                                         ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'85d9edb7-16c2-4686-9ccb-37af29aee9f8', 2, N'Dolostone                                                                                                                                                                                                                                                      ', 6)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'7f5a1f05-802f-42e4-8b88-3e4302378448', 2, N'Rhyolite                                                                                                                                                                                                                                                       ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'e2a02c62-431a-4b29-bacb-4a9483195667', 1, N'Alkali rhyolite                                                                                                                                                                                                                                                ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'c00acd3b-5d51-474a-aee1-5043d4b460c0', 7, N'Quartz monzonite                                                                                                                                                                                                                                               ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'2023a08c-6cf5-48a7-8175-515145133bc3', 8, N'Metaconglomerate                                                                                                                                                                                                                                               ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'40bdfa05-252d-41bd-9c58-555918d0e933', 19, N'Peridotite                                                                                                                                                                                                                                                     ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'37bfcdd1-69ab-4242-aeed-58740038d205', 4, N'Dacite                                                                                                                                                                                                                                                         ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'4ee58598-10f4-423a-bec0-622817ce68c9', 12, N'Alkai-feldspar syenite                                                                                                                                                                                                                                         ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'1263ed03-5da8-4c5e-a103-63fbe129156c', 1, N'Slate                                                                                                                                                                                                                                                          ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'57e52639-4769-41dc-b8b7-65fc5d9b235c', 3, N'Halite                                                                                                                                                                                                                                                         ', 6)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'a2992bb5-07e9-4aa7-a125-67a78451ea92', 14, N'Monzonite                                                                                                                                                                                                                                                      ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'8fc630d4-4417-45d3-8b65-6a52f3113044', 2, N'Breccia                                                                                                                                                                                                                                                        ', 5)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'ca05e7e0-4e72-4a5e-964b-72747b2d3333', 13, N'Latite-basalt                                                                                                                                                                                                                                                  ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'86f08037-9fdd-46a8-bbcb-79e8d5226191', 5, N'Quartzite                                                                                                                                                                                                                                                      ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'e86d2780-abd3-4a85-87f0-810d3c23f5d6', 8, N'Quartz latite                                                                                                                                                                                                                                                  ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'c384368d-bebe-44c7-b2b4-81a3dc6dc743', 5, N'Coal                                                                                                                                                                                                                                                           ', 6)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'80362464-3dac-45f0-a4f0-832edc54f46d', 3, N'Sandstone                                                                                                                                                                                                                                                      ', 5)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'327ea505-fc94-4fc4-9383-86f736b2e017', 7, N'Hornfels                                                                                                                                                                                                                                                       ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'5938ba5e-2f24-42a6-8644-88445d851a6b', 6, N'Alkali quartz trachyte                                                                                                                                                                                                                                         ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'8c398fd2-5ac3-4b95-8910-89fa06e1ef5a', 16, N'Monzogabbro                                                                                                                                                                                                                                                    ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'536f711d-b6a3-425c-8354-8e971b9fadc7', 5, N'Shale                                                                                                                                                                                                                                                          ', 5)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'3f46db3e-8a3c-491c-bcb7-947787c701d1', 5, N'Quartz Andesite                                                                                                                                                                                                                                                ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'cf3317aa-486c-452d-b495-96241b505f4c', 3, N'Rhyodacite                                                                                                                                                                                                                                                     ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'ff1aacbb-2548-47aa-a823-9e4d245425a4', 1, N'N/A                                                                                                                                                                                                                                                            ', 1)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'4b2b3076-5fb5-42fa-8953-9e7b0ad2b8e0', 3, N'Vitric Tuff                                                                                                                                                                                                                                                    ', 4)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'a01f4779-93f2-4cef-b8b7-9f91a7048035', 7, N'Quartz trachyte                                                                                                                                                                                                                                                ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'fba78891-e11c-4dce-a4be-a07cce64345d', 4, N'Lithic Tuff                                                                                                                                                                                                                                                    ', 4)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'e569d12d-6f83-4cbd-ae20-a85db109e024', 2, N'Volcanic Breccia                                                                                                                                                                                                                                               ', 4)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'a716d782-f358-4af4-81ff-bd154d8dc174', 4, N'Siltstone                                                                                                                                                                                                                                                      ', 5)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'085c764e-d72c-4dd7-a4dd-bfea2119b72b', 11, N'Trachyte                                                                                                                                                                                                                                                       ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'4546cd9d-dc19-4c65-b2a0-c9beb0d4b08c', 1, N'Granite                                                                                                                                                                                                                                                        ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'0fc556a4-6693-4d41-bd79-cd7092947a04', 9, N'Andesite                                                                                                                                                                                                                                                       ', 3)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'c7b2cf24-ebd7-4d84-8c31-d11aafa6cdd5', 1, N'Agglomerate                                                                                                                                                                                                                                                    ', 4)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'fb5e27e9-1aec-4125-9992-d56fe6f27420', 4, N'Gypsum                                                                                                                                                                                                                                                         ', 6)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'1c2c547d-a30c-48a6-af92-d77370905ed0', 3, N'Granodiorite                                                                                                                                                                                                                                                   ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'd7100f27-e822-402c-be58-d77ca12e1d0e', 11, N'Quartz gabbro                                                                                                                                                                                                                                                  ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'120440bc-b9c0-4beb-ab09-dc5bbbbcf7f6', 5, N'Alkali-feldspar quartz syenite                                                                                                                                                                                                                                 ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'b66a09e0-0d5c-47d6-a6b6-de33b095dc7e', 6, N'Marbble                                                                                                                                                                                                                                                        ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'029fb608-0ff5-434e-8398-e1c6efe3ce8e', 10, N'Quartz diorite                                                                                                                                                                                                                                                 ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'a3d7e78a-7ef0-4296-b4a3-e3d0eb24d110', 9, N'Mylonite                                                                                                                                                                                                                                                       ', 7)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'4fa0fb6d-7822-4586-899d-e7443cc078ae', 13, N'Syenite                                                                                                                                                                                                                                                        ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'd39561ae-4cb5-47cc-9ea3-e897680e5622', 2, N'Alkali-feldspar granite                                                                                                                                                                                                                                        ', 2)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'e006a46d-0247-403a-b438-f1e0095d884e', 1, N'Limestone                                                                                                                                                                                                                                                      ', 6)
GO
INSERT [Characterization].[SubGroup] ([id], [code], [description], [groupGeneticCode]) VALUES (N'cfc2decb-6749-4289-9280-f44c26938bbe', 5, N'Cyrstal Tuff                                                                                                                                                                                                                                                   ', 4)
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'e29576e8-066d-417e-8e27-2ee9ae490a2c', 5, N'Crystalline                                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'a9ff8c0d-421e-4890-bb2d-7d84ad7c4891', 4, N'Porphyritic                                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'9a112af7-459d-4a02-ad8b-9e8ced895c1d', 1, N'Equigranular                                                                                                                                                                                                                                                   ')
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'05ba3ea8-f837-437c-9f3a-afc4fa1589a6', 7, N'Aphanitic                                                                                                                                                                                                                                                      ')
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'37d02cba-d2f5-4700-8297-befe7de8d8b5', 6, N'Cryptocrystalline                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'9ab7a979-3524-4399-b4f1-d715245f0527', 2, N'Inequigranular                                                                                                                                                                                                                                                 ')
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'f49558f3-b2c0-4c27-8bf2-e25725f7e4ce', 8, N'Pyroclastic                                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[Texture] ([id], [code], [description]) VALUES (N'53cf5374-28da-4bfc-8dcc-f2b53b5e79d7', 3, N'Megacrystic                                                                                                                                                                                                                                                    ')
GO
INSERT [Characterization].[Water] ([id], [code], [description]) VALUES (N'9c6a4c35-41e2-4c27-94b2-5b7d4f941648', 3, N'Wet [10-25 l/min]                                                                                                                                                                                                                                              ')
GO
INSERT [Characterization].[Water] ([id], [code], [description]) VALUES (N'd6e6f0ad-9ca4-4031-a37a-b9481615ef02', 5, N'Flowing [>125 l/min]                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Water] ([id], [code], [description]) VALUES (N'defcf9e7-8cbe-4b44-9fd6-c5bf810df960', 1, N'Dry [none]                                                                                                                                                                                                                                                     ')
GO
INSERT [Characterization].[Water] ([id], [code], [description]) VALUES (N'9bb29464-44ed-4beb-b00c-c8aa7e0792b2', 2, N'Damp [<10 l/min]                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[Water] ([id], [code], [description]) VALUES (N'64bd892d-f472-4c1b-b8e4-cfaca13bbcbf', 4, N'Dripping [25-125 l/min]                                                                                                                                                                                                                                        ')
GO
INSERT [Characterization].[WaterQuality] ([id], [code], [description]) VALUES (N'adf5e5ea-9b06-492a-8ca5-220897c617e6', 4, N'Sediment content                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[WaterQuality] ([id], [code], [description]) VALUES (N'd369bf81-1fd1-44d2-8bd4-59e3fdbef098', 3, N'Turbid                                                                                                                                                                                                                                                         ')
GO
INSERT [Characterization].[WaterQuality] ([id], [code], [description]) VALUES (N'44d2d64e-5a95-4aa2-9c89-9fa08c8190b3', 5, N'Smell                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[WaterQuality] ([id], [code], [description]) VALUES (N'10eba3fa-ace9-467c-b52c-cba906d0dde9', 1, N'N/A                                                                                                                                                                                                                                                            ')
GO
INSERT [Characterization].[WaterQuality] ([id], [code], [description]) VALUES (N'6df06597-5069-426a-9ee0-dea65218def8', 2, N'Clear                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Weathering] ([id], [code], [description]) VALUES (N'11e9c682-578f-48e7-8a1b-210d431e3faf', 1, N'Fresh                                                                                                                                                                                                                                                          ')
GO
INSERT [Characterization].[Weathering] ([id], [code], [description]) VALUES (N'1ec942e5-d956-4442-8b19-22ff0faf3aa8', 2, N'Slightly weathered                                                                                                                                                                                                                                             ')
GO
INSERT [Characterization].[Weathering] ([id], [code], [description]) VALUES (N'd66e6774-d244-4c1b-93af-2921944998a9', 6, N'Residual soil                                                                                                                                                                                                                                                  ')
GO
INSERT [Characterization].[Weathering] ([id], [code], [description]) VALUES (N'250bca64-cd06-44af-b5e7-3f781ea791f7', 5, N'Completely weathered                                                                                                                                                                                                                                           ')
GO
INSERT [Characterization].[Weathering] ([id], [code], [description]) VALUES (N'59272cc2-b779-47e6-9d6c-8916b2a4412d', 4, N'Highly weathered                                                                                                                                                                                                                                               ')
GO
INSERT [Characterization].[Weathering] ([id], [code], [description]) VALUES (N'070f2bdd-a6d1-4728-a18c-a3a9a2f69d5a', 3, N'Moderately weathered                                                                                                                                                                                                                                           ')
GO
